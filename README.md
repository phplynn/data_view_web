## 新版本已经发布，欢迎查看
https://github.com/ggymm/data-view-web

## 数据可视化平台

### 前端介绍

图表基于echarts4

UI部分基于element-ui


### 讨论群

177745111


#### 开发

```shell
npm install
npm run dev
```



#### 支持图表

1. 散点图
   1. 气泡散点图
   2. 散点地图
2. 折线图
   1. 标准折线图
   2. 时间轴折线图
   3. 堆叠折线图
   4. 堆叠面积图
3. 柱状图
   1. 渐变柱状图
   2. 水平柱状图
   3. 堆叠柱状图
   4. 复合柱状图
   5. 双向柱状图
4. 地图
   1. 中国地图
   2. 世界地图
   3. 省份地图
5. 饼状图
   1. 普通饼图
   2. 环形饼图
   3. 2D饼图
   4. 环形百分比图
   5. 环形饼图列表
6. 雷达图
   1. 基础雷达图
7. 热力图
   1. 基础热力图
   2. 省份热力图
   3. 热力地图
   4. 颜色的离散映射
8. 关系图
   1. 关系图1
   2. 关系图2
   3. 关系图3
   4. 关系图4
   5. 关系图5
9. 其他
   1. 词云
   2. 轮播列表
   3. 计数器
   4. 标题文本
   5. 进度条
   6. 图片
   7. 时间
   8. 仪表盘



### 后端服务介绍

[后端服务开源地址](https://gitee.com/1967988842/data_view)

1. 使用Go编写的后端服务
2. web服务使用iris框架
3. 数据库访问层使用xorm
4. models/charts目录下是对所有图表的数据格式化实现，负责将数据库数据查询出赋值给前端图表options



### 项目示例



#### 数据源管理

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144310_f887ec57_673473.png "微信截图_20210515144254.png")



#### 图表列表

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144320_3dc500d4_673473.png "微信截图_20210515143807.png")



#### 图表编辑

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144337_c1095cd5_673473.png "微信截图_20210515144327.png")



#### 图表配置
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/132101_598893fa_673473.png "微信截图_20210521132021.png")



#### 图表预览

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144413_0375d5bd_673473.png "微信截图_20210515144401.png")



### 示例图表

一共有四个图表模板

[数据库地址](https://gitee.com/1967988842/data_view/blob/master/_doc/data_view.sql)

[模板所需数据源地址](https://gitee.com/1967988842/data_view/blob/master/_doc/data_view_template.sql)

导入数据库后记得修改data_source表中的模板数据源的账号和密码等数据



#### 图表模板1

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144509_0fb58b7f_673473.png "云资源监控-结果.png")



#### 图表模板2

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144519_4f797296_673473.png "企业实时销售数据-结果.png")



#### 图表模板3

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144533_9568abe8_673473.png "通用模板1-结果.png")



#### 图表模板4

![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/144623_7a41f682_673473.png "腾讯云服务监控.png")